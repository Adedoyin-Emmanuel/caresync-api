"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controllers_1 = require("../controllers");
const middlewares_1 = require("./../middlewares");
const symptomsCheckerRouter = express_1.default.Router();
symptomsCheckerRouter.post("/", [middlewares_1.useAuth, (0, middlewares_1.useCheckRole)("user")], controllers_1.SymptomsChecker.checkSymptoms);
exports.default = symptomsCheckerRouter;
