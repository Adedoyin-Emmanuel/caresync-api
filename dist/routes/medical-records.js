"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const middlewares_1 = require("../middlewares");
const controllers_1 = require("../controllers");
const medicalRecordRouter = express_1.default.Router();
medicalRecordRouter.post("/", [middlewares_1.useAuth, (0, middlewares_1.useCheckRole)("user")], controllers_1.MedicalRecordController.createMedicalRecord);
medicalRecordRouter.get("/", [middlewares_1.useAuth], controllers_1.MedicalRecordController.getAllMedicalRecords);
medicalRecordRouter.get("/me", [middlewares_1.useAuth], controllers_1.MedicalRecordController.getCurrentUserMedicalRecords);
medicalRecordRouter.get("/:id", [middlewares_1.useAuth], controllers_1.MedicalRecordController.getMedicalRecordById);
medicalRecordRouter.put("/:id", [middlewares_1.useAuth, (0, middlewares_1.useCheckRole)("user")], controllers_1.MedicalRecordController.updateMedicalRecord);
medicalRecordRouter.delete("/hospital-access", controllers_1.MedicalRecordController.deleteMedicalRecordAccessByHospitalId);
medicalRecordRouter.delete("/:id", [middlewares_1.useAuth, (0, middlewares_1.useCheckRole)("user")], controllers_1.MedicalRecordController.deleteMedicalRecord);
exports.default = medicalRecordRouter;
