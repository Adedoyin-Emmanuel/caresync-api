"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const sdk_1 = __importDefault(require("@anthropic-ai/sdk"));
const utils_1 = require("./../utils");
class SymptomsChecker {
    static checkSymptoms(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const requestSchema = joi_1.default.object({
                    symptoms: joi_1.default.string().required(),
                    birthYear: joi_1.default.number().required(),
                    gender: joi_1.default.string().valid("male", "female", "child").required(),
                });
                const { error, value } = requestSchema.validate(yield req.body);
                if (error)
                    return (0, utils_1.response)(res, 400, error.details[0].message);
                const { symptoms, birthYear, gender } = value;
                const ANTHROPIC_API_KEY = process.env.CLAUDE_AI_SECRET_KEY;
                const anthropic = new sdk_1.default({ apiKey: ANTHROPIC_API_KEY });
                const CUSTOM_PROMPT = `

  You are a helpful AI symptoms checker named Caresync,
  an advanced AI dedicated to helping users identify potential health conditions based on their symptoms.
  You must provide accurate, relevant, and helpful information only about health diagnoses, healthcare recommendations, diseases and treatments, drugs, possible disease, rate of urgency, medical procedures, and related topics within the healthcare domain.
  The user must provide you with their symptoms, then they provide you with their age and their gender which can only be Male, Female or Child.
  You must respond in simple, concise, and understandable language that any user can comprehend.
  If a user asks a question or initiates a discussion that is not directly related to healthcare, medical topics or symptoms , diseases etc.
  Do not provide an answer or engage in the conversation. Instead, politely redirect their focus back to the healthcare domain and its related content.
  If a user inquires about the creator of Caresync, respond with: "The creator of  Caresync  is Adedoyin Emmanuel Adeniyi, a Software Engineer."
  Your expertise is limited to healthcare, medical diagnosis, treatments, and related topics, and you must not provide any information on topics outside the scope of that domain.
  If a user inquires about the symptoms of a specific disease, you must provide accurate information about the symptoms of that disease.
  You must also tell the user to not hesitate to book an appointment with an hospital from the appointment tab on the dashboard.
  Additionally, you must only answer and communicate in English language, regardless of the language used by the user

  `;
                const USER_PROMPT = `
    I've the following symptoms ${symptoms}

     I'm a ${gender} and I was born in ${birthYear}
    `;
                const responseData = yield anthropic.completions.create({
                    model: "claude-2.1",
                    max_tokens_to_sample: 1024,
                    prompt: `${sdk_1.default.HUMAN_PROMPT} ${CUSTOM_PROMPT} ${USER_PROMPT}${sdk_1.default.AI_PROMPT}`,
                });
                return (0, utils_1.response)(res, 200, "Caresync data fetched successfully", responseData);
            }
            catch (error) {
                console.log(error);
                return (0, utils_1.response)(res, 500, "An error occured");
            }
        });
    }
}
exports.default = SymptomsChecker;
