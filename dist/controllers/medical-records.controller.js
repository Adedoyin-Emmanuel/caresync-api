"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const utils_1 = require("./../utils");
const models_1 = require("../models");
class MedicalRecordController {
    static createMedicalRecord(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestSchema = joi_1.default.object({
                userId: joi_1.default.string().required(),
                symptoms: joi_1.default.string().required().max(2500),
                diagnosis: joi_1.default.string().max(25000).required(),
            });
            const { error, value } = requestSchema.validate(req.body);
            if (error)
                return (0, utils_1.response)(res, 400, error.details[0].message);
            const user = yield models_1.User.findOne({ _id: value === null || value === void 0 ? void 0 : value.userId });
            if (!user)
                return (0, utils_1.response)(res, 404, "User with given id does not exist");
            const medicalRecord = yield models_1.MedicalRecord.create(value);
            yield models_1.User.findByIdAndUpdate(value.userId, { $push: { medicalRecords: medicalRecord.id } }, { new: true });
            return (0, utils_1.response)(res, 201, "Medical record created successfully", medicalRecord);
        });
    }
    static getMedicalRecordById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var _a;
            const hospitalId = (_a = req.query) === null || _a === void 0 ? void 0 : _a.hospitalId;
            const role = req.userType;
            const requestSchema = joi_1.default.object({
                id: joi_1.default.string().required(),
            });
            const { error, value } = requestSchema.validate(req.params);
            if (error)
                return (0, utils_1.response)(res, 400, error.details[0].message);
            if (role == "hospital" && !hospitalId)
                return (0, utils_1.response)(res, 400, "Hospital id is required");
            const medicalRecord = yield models_1.MedicalRecord.findById(value === null || value === void 0 ? void 0 : value.id);
            if (!medicalRecord)
                return (0, utils_1.response)(res, 404, "Medical record with given if not found");
            const user = yield models_1.User.findById(medicalRecord === null || medicalRecord === void 0 ? void 0 : medicalRecord.userId);
            if (!user)
                return (0, utils_1.response)(res, 404, "User with given id not found");
            const hasAccess = user.medicalRecordsAccess.some((access) => {
                //console.log(`access id is ${access?._id} hospital id is ${hospitalId}`);
                return access._id.toString() === hospitalId;
            });
            // check if the hospital has access to user medical record
            if (role == "hospital" && !hasAccess)
                return (0, utils_1.response)(res, 403, "Hospital doesn't have access to view user medical record");
            return (0, utils_1.response)(res, 200, "Medical record retrived successfully", medicalRecord);
        });
    }
    static getAllMedicalRecords(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userId = req.query.userId;
            const hospitalId = req.query.hospitalId;
            if (!userId)
                return (0, utils_1.response)(res, 400, "User id is required");
            if (!hospitalId)
                return (0, utils_1.response)(res, 400, "Hospital id is required");
            const user = yield models_1.User.findById(userId);
            if (!user)
                return (0, utils_1.response)(res, 404, "User with given id not found");
            const hasAccess = user.medicalRecordsAccess.some((access) => {
                //console.log(`access id is ${access?._id} hospital id is ${hospitalId}`);
                return access._id.toString() === hospitalId;
            });
            // check if an hopsital has access to view the user medical record
            console.log(hasAccess);
            if (!hasAccess)
                return (0, utils_1.response)(res, 403, "Hospital doesn't have access to view user medical records");
            const queryConditions = userId ? { userId: userId } : {};
            const medicalRecords = yield models_1.MedicalRecord.find(queryConditions)
                .sort({ createdAt: -1 })
                .exec();
            if (medicalRecords.length == 0)
                return (0, utils_1.response)(res, 200, "Medical records retrived successfully", []);
            return (0, utils_1.response)(res, 200, "Medical records retrived successfully", medicalRecords);
        });
    }
    static getCurrentUserMedicalRecords(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const userId = req.user._id;
            console.log(userId);
            const currentUserMedicalRecords = yield models_1.MedicalRecord.find({
                userId,
            })
                .sort({ createdAt: -1 })
                .exec();
            if (!userId)
                return (0, utils_1.response)(res, 404, "User with given id not found");
            if (!currentUserMedicalRecords)
                return (0, utils_1.response)(res, 404, "No medical records found", []);
            return (0, utils_1.response)(res, 200, "Medical Response retrived successfully", currentUserMedicalRecords);
        });
    }
    static updateMedicalRecord(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestSchema = joi_1.default.object({
                symptoms: joi_1.default.string().required().max(2500),
                diagnosis: joi_1.default.string().max(25000).required(),
            });
            const { error, value } = requestSchema.validate(req.body);
            if (error)
                return (0, utils_1.response)(res, 400, error.details[0].message);
            const id = req.params.id;
            if (!id)
                return (0, utils_1.response)(res, 400, "Id is required");
            const updatedMedicalRecord = yield models_1.MedicalRecord.findByIdAndUpdate(id, value);
            return (0, utils_1.response)(res, 200, "Medical record updated successfully", updatedMedicalRecord);
        });
    }
    static deleteMedicalRecord(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestSchema = joi_1.default.object({
                id: joi_1.default.string().required(),
            });
            const { error, value } = requestSchema.validate(req.params);
            if (error)
                return (0, utils_1.response)(res, 400, error.details[0].message);
            const deletedMedicalRecord = yield models_1.MedicalRecord.findByIdAndDelete(value === null || value === void 0 ? void 0 : value.id);
            if (!deletedMedicalRecord)
                return (0, utils_1.response)(res, 404, "Medical record with given id not found");
            const userId = deletedMedicalRecord === null || deletedMedicalRecord === void 0 ? void 0 : deletedMedicalRecord.userId;
            yield models_1.User.findByIdAndUpdate(userId, {
                $pull: { medicalRecords: value === null || value === void 0 ? void 0 : value.id },
            });
            return (0, utils_1.response)(res, 200, "Medical record deleted successfully", deletedMedicalRecord);
        });
    }
    static deleteMedicalRecordAccessByHospitalId(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const requestSchema = joi_1.default.object({
                hospitalId: joi_1.default.string().required(),
                userId: joi_1.default.string().required(),
            });
            const { error, value } = requestSchema.validate(req.query);
            if (error)
                return (0, utils_1.response)(res, 400, error.details[0].message);
            const { hospitalId, userId } = value;
            const hospital = yield models_1.Hospital.findById(hospitalId);
            if (!hospital)
                return (0, utils_1.response)(res, 404, "Hospital with given id not found");
            const user = yield models_1.User.findById(userId);
            if (!user)
                return (0, utils_1.response)(res, 404, "User with given id not found");
            yield models_1.User.findByIdAndUpdate(userId, {
                $pull: { medicalRecordsAccess: hospitalId },
            });
            return (0, utils_1.response)(res, 200, `${hospital.clinicName} has been removed from your medical record access`);
        });
    }
}
exports.default = MedicalRecordController;
